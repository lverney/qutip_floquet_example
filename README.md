This is an example notebook for using Floquet theory with QuTiP.

The `floquet.py` file is a fixed version of [the QuTiP original
file](https://github.com/qutip/qutip/blob/845361425f2538a37871d118aee78f6938ed3e86/qutip/floquet.py),
waiting for the fixes to be merged upstream. It is distributed under the
original QuTiP new BSD license.

The notebook itself is distributed under MIT license.
